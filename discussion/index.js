// Array Methods

// Mutator methods - methods that modify values

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current array: ");
console.log(fruits);

// push()
// Adds an element in the end of an array AND returns the array's length
// Syntax: arrayName.push()

fruits.push("Mango");
console.log("Mutated array from push method.");
console.log(fruits);

// push()
// Adds an element in the end of an array AND returns the array's length
// Syntax: arrayName.push()
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method.");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method.");
console.log(fruits);

// pop()
/* 
    - Removes the last element in an array AND returns the removed element
    Syntax: arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit); // Guava
console.log("Mutated array from pop method.");
console.log(fruits);

// unshift
/* 
    - Adds one or more elements at the beginning at the aray
    Syntax: 
    arrayName.unshift("elementA")
    arrayName.unshift("elementA", "elementB")
*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method.");
console.log(fruits);

// shift
/* 
    - Removes an element at the beginning of an array AND returns the removed element
    Syntax:
    arrayName.shift("elementA")
    arrayName.shift("elementA", "elementB")
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method.");
console.log(fruits);

// splice
/* 
    - Simultaneously removes elements from a specified index and adds elements
    Syntax: arrayName.splice(startingIndex, deleteCount)
*/

fruits.splice(2, 3, "Calamansi", "Lime", "Cherry");
console.log("Mutated array from splice method.");
console.log(fruits);

// sort()
/* 
    - Rearranges the array elements in alphanumeric order
    Syntax: arrayName.sort();
 */
fruits.sort();
console.log("Mutated array from sort method.");
console.log(fruits);

// reverse()
/* 
    - Reverses the order of array elements
    Syntax: arrayName.reverse()
*/
fruits.reverse();
console.log("Mutated array from reverse method.");
console.log(fruits);

// Non-Mutator Methods
/* 
    Non-mutator methods are functions that do not modify or change an array after they're created 

    These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays
*/
let countries = ["US", "PH", "CAN", "SG", "PH", "FR", "GE", "CH", "KR"];

//indexOf()
/* 
    Returns the index number of the first matching element found in an array.
    If no match found, result will be -1.
    Syntax:
    - arrayName.indexOf(searchValue);
    - arrayName.indexOf(searchValue, fromIndex);
*/
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

// lastIndexOf()
/* 
    - Returns the index number of the last matching element found in an array
    - The search process will be done from the last element proceeding to the first element
    Syntax:
        - arrayName.lastIndexOf(searchValue)
        - arrayName.lastIndexOf(searchValue, fromIndex)
*/
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 3);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
/* 
    - Portion/slices elements from an array AND returns a new array 
    Syntax:
    arrayName.slice(startingIndex);
    arrayName.slice(startingIndex, endingIndex)
*/

let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

// slicing elements from the last element
let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()
// Returns an array as string separated by commas
let stringArray = countries.toString();
console.log(stringArray);

// concat()
// Combines 2 arrays and returns the combined result
let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

let tasks2 = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log("Result from concat method:");
console.log(tasks2);

let tasks3 = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method:");
console.log(tasks3);

// join()
// Returns an array as string separated by specific separator string
let users = ["John", "Juan", "Dudong"];
console.log(users.join(" >> "));

// Iteration methods
/*
	- Iteration methods are loops designed to perform repetitive tasks on arrays
	- Iteration methods loops over all items in an array.
	- Useful for manipulating array data resulting in complex tasks
	- Array iteration methods normally work with a function supplied as an argument
	- How these function works is by performing tasks that are pre-defined within an array's method.
*/

// foreach()
/*
    - Similar to a for loop that iterates on each array element.
    - For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
    - Variable names for arrays are normally written in the plural form of the data stored in an array
    - It's common practice to use the singular form of the array content for parameter names used in array loops
    - forEach() does not return anything.
    - Syntax
        arrayName.forEach(function(indivElement) {
            statement
        })
*/

tasks2.forEach(function (task) {
  console.log(task);
});

// Using foreach() for filtering all elements
filteredTasks = [];
tasks2.forEach(function (task) {
  if (task.length > 10) {
    filteredTasks.push(task);
  }
});

console.log(filteredTasks);

// map()
/* 
    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
    - This is useful for performing tasks where mutating/changing the elements are required
    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    Syntax:
        let/const resultArray = arrayName.map(function(indivElement))
*/
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function (number) {
  return number * number;
});

console.log("Original Array:");
console.log(numbers);
console.log("Result of map method:");
console.log(numberMap);

// every
// checks if all statements in an array meet the given condition
let allValid = numbers.every(function (number) {
  return number < 2;
});
console.log(allValid);

// some
// checks if some statement in an array meet the given condition
let someValid = numbers.some(function (number) {
  return number < 2;
});
console.log(someValid);

// filter()
// Returns new array that contains elements which meets the given condition
let filterValid = numbers.filter(function (number) {
  return number < 2;
});

console.log(filterValid);

// includes()
// includes method checks if the argument passed can be found in the array
let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

/* 
    - Methods can be "chained"
*/

let filteredProducts = products.filter(function (product) {
  return product.toLowerCase().includes("a");
});

console.log(filteredProducts);

// reduce()
/* 
    - Evaluates elements from left to right and returns the array into a single value
    Syntax: 
    let/const resultArray = arrayName.reduce(accumulator, currentValue)
        return expression

    - The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop

            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
*/

let iteration = 0;
let reducedArray = numbers.reduce(function (acc, cur) {
  console.warn("Current iteration: " + ++iteration);
  console.log("Accumulator: " + acc);
  console.log("Current Value: " + cur);

  return acc + cur;
});

console.log(reducedArray);

// reducing string arrays
let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function (x, y) {
  return x + " " + y;
});
console.log("Result of reduce method: " + reducedJoin);
